## Stage 0 - Build
FROM golang:1.13.7-buster as build


WORKDIR /build

COPY ./ .

RUN go mod download
RUN go build .

## Stage 1 - Execution

FROM debian:buster

WORKDIR /app

ENV REPOSITORY_DIRECTORY="/repository"
ENV LISTEN_ADDRESS=":8080"

COPY --from=build /build/mvnproxy .

EXPOSE 8080
VOLUME /repository

CMD ["./mvnproxy"]