package main

import (
    "fmt"
    "log"
    "net/http"
    "path/filepath"
    "strings"
    "os"
    "io"
)

var RepositoryPath = "/repository"
var ListenAddr = "0.0.0.0:8080"

func artifactDirectory(groupId string, artifactId string, version string) string {

    groupDirectory := strings.Replace(groupId, ".", "/", -1)

    return fmt.Sprintf("%s/%s/%s/%s", RepositoryPath, groupDirectory, artifactId, version)
}

func artifactFilePath(groupId string, artifactId string, version string, file string) string {
    return fmt.Sprintf("%s/%s", artifactDirectory(groupId, artifactId, version), file)
}

func createArtifactDirectory(groupId string, artifactId string, version string) {
    os.MkdirAll(artifactDirectory(groupId, artifactId, version), os.ModePerm)
}

func uploadFile(groupId string, artifactId string, version string, file string, in io.Reader) (err error) {

    filename := artifactFilePath(groupId, artifactId, version, file)

    out, err := os.Create(filename)

    if err != nil {
        return
    }
    defer out.Close()

    _, err = io.Copy(out, in)

    if err != nil {
        return
    }

    return
}

func handler(w http.ResponseWriter, r *http.Request) {

    method := r.Method
    path := strings.Replace(r.URL.Path, "..", "", -1)

    directory := filepath.Dir(path)
    file := filepath.Base(path)
        
    remoteAddr := r.RemoteAddr
    parts := strings.Split(directory, "/")
    groupId := strings.Join(parts[2:len(parts)-2], ".")
    artifactId := parts[len(parts)-2]
    version := parts[len(parts)-1]

    if method == "PUT" {

        log.Printf("[%s] PUT groupId=%s, artifactId=%s, version=%s, file: %s\t", remoteAddr, groupId, artifactId, version, file)
        createArtifactDirectory(groupId, artifactId, version)
        
        err := uploadFile(groupId, artifactId, version, file, r.Body)

        if err != nil {
            w.WriteHeader(http.StatusInternalServerError)
            log.Printf("FAILED: %s", err.Error())
        } else {
            w.WriteHeader(http.StatusCreated)
            log.Printf("SUCCESS")
        }
    } else if method == "GET" {

        log.Printf("[%s] GET groupId=%s, artifactId=%s, version=%s, file: %s\t", remoteAddr, groupId, artifactId, version, file)

        requestedFile := artifactFilePath(groupId, artifactId, version, file)
        
        if _, err := os.Stat(requestedFile); err != nil {
            log.Printf("FAILED: %s", err.Error())
            w.WriteHeader(http.StatusNotFound)
        } else {
            log.Printf("SUCCESS")
            http.ServeFile(w, r, requestedFile)
        }

    }
}

func main() {

    inRepo := os.Getenv("REPOSITORY_DIRECTORY")
    inAddr := os.Getenv("LISTEN_ADDR")

    if len(inRepo) > 0 {
        RepositoryPath = inRepo
    }

    if len(inAddr) > 0 {
        ListenAddr = inAddr
    }

    log.Printf("*********** Maven Repository *********************")
    log.Printf(" Addr: %s", ListenAddr)
    log.Printf(" Path: %s", RepositoryPath)
    log.Printf("**************************************************\n\n")

    http.HandleFunc("/maven2/", handler)

    log.Fatal(http.ListenAndServe(ListenAddr, nil))
}
